from highcampers.extensions import mail
from highcampers.blueprints.contact.tasks import deliver_contact_email

class TestTasks(object):
    def test_deliver_contact_email(self):

        form = {
            'email': 'foo@bar.com',
            'message': 'Good to see you'
        }
        with mail.record_messages() as outbox:
            deliver_contact_email(form.get('email'), form.get('message'))

            assert len(outbox) == 1
            assert form.get('message') in outbox[0].body
