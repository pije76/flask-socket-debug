Hi {{ user.username }},

You are just a step away. Verify your email from below and start posting in the world of travellers.

{{ url_for('user.email_verification', confirmation_token=confirmation_token, _external=True) }}

You are receiving this email because you signed up with Highcampers

Thanks,
Aswin
Highcampers Community Manager