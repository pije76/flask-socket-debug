from flask import (
    Blueprint,
    flash,
    redirect,
    request,
    url_for,
    render_template)
from flask_login import login_required, logout_user, login_user, current_user
from lib.safe_next_url import safe_next_url
from highcampers.blueprints.user.decorators import anonymous_required

from highcampers.blueprints.user.forms import LoginForm, SignupForm, \
    UpdateCredentialsForm, BeginPasswordReset, PasswordResetForm, UpdatePasswordForm
from highcampers.blueprints.user.models import User

user = Blueprint('user', __name__, template_folder='templates')


@user.route('/login', methods=['GET', 'POST'])
@anonymous_required()
def login():
    form = LoginForm(next=request.args.get('next'))

    if form.validate_on_submit():
        u = User.find_by_identity(request.form.get('identity'))

        if u and u.authenticated(password=request.form.get('password')):
            if login_user(u, remember=True) and u.is_active():
                u.update_activity_tracking(request.remote_addr)

                next_url = request.form.get('next')
                if next_url:
                    return redirect(safe_next_url(next_url))

                return redirect(url_for('user.settings'))
            else:
                flash('This account has been disabled.', 'error')

        else:
            flash('Identity or password is incorrect', 'error')

    return render_template('user/login.html', form=form)


@user.route('/signup', methods=['GET', 'POST'])
@anonymous_required()
def signup():
    form = SignupForm()

    if form.validate_on_submit():
        u = User()

        form.populate_obj(u)
        u.password = User.encrypt_password(request.form.get('password'))
        u.save()

        if login_user(u):

            email_verification = u.send_email_verification(u.email)
            from highcampers.blueprints.user.tasks import deliver_welcome_email
            deliver_welcome_email.delay(request.form.get('email'),
                                        current_user.username)

            flash('Complete your registration by confirming your email address', 'warning')
            return redirect(url_for('user.settings'))

    return render_template('user/signup.html', form=form)


@user.route('/email-verification')
def email_verification():
    u = User.deserialize_token(request.args.get('confirmation_token'))

    if u is None:
        flash('Your email confirmation token has expired or tampered with.',
              'error')
        return redirect(url_for('user.begin_password_reset'))

    if login_user(u):
        u.active = True
        u.save()
        flash('Your account is now verified.', 'success')

        return redirect(url_for('user.settings'))


@user.route('/settings', methods=['GET', 'POST'])
@login_required
def settings():
    form = UpdateCredentialsForm(current_user, uid=current_user.id)

    if form.validate_on_submit():
        current_user.username = request.form.get('username')
        current_user.email = request.form.get('email')
        current_user.fullname = request.form.get('fullname', '')

        current_user.save()
        flash('Your profile settings have been updated', 'success')

        return redirect(url_for('user.settings'))

    return render_template('user/settings.html', form=form)


@user.route('/settings/update_password', methods=['GET', 'POST'])
@login_required
def update_password():
    form = UpdatePasswordForm(current_user, uid=current_user.id)

    if form.validate_on_submit():
        new_password = request.form.get('password', '')

        if new_password:
            current_user.password = User.encrypt_password(new_password)

        current_user.save()
        flash('Your password has been updated', 'success')

        return redirect(url_for('user.settings'))

    return render_template('user/update_password.html', form=form)


@user.route('/account/begin_password_reset', methods=['GET', 'POST'])
@anonymous_required()
def begin_password_reset():
    form = BeginPasswordReset()

    if form.validate_on_submit():
        u = User.initialize_password_reset(request.form.get('identity'))

        flash('An email has been sent to {0}.'.format(u.email), 'success')
        return redirect(url_for('user.login'))

    return render_template('user/begin_password_reset.html', form=form)


@user.route('/account/password_reset', methods=['GET', 'POST'])
@anonymous_required()
def password_reset():
    form = PasswordResetForm(reset_token=request.args.get('reset_token'))

    if form.validate_on_submit():
        u = User.deserialize_token(request.form.get('reset_token'))

        if u is None:
            flash('Your reset token has expired or tampered with.',
                  'error')
            return redirect(url_for('user.begin_password_reset'))

        form.populate_obj(u)
        u.password = User.encrypt_password(request.form.get('password'))
        u.save()

        if login_user(u):
            flash('Your password has been reset.', 'success')
            return redirect(url_for('user.settings'))

    return render_template('user/password_reset.html', form=form)

@user.route('/logout')
@login_required
def logout():
    logout_user()
    flash('You have been logged out.', 'success')
    return redirect(url_for('page.home'))
