from flask import (
    Blueprint,
    render_template)
from flask_login import login_required
from highcampers.blueprints.admin.models import Dashboard
from highcampers.blueprints.user.decorators import role_required

admin = Blueprint('admin', __name__, template_folder='templates', url_prefix='/admin')


@admin.before_request
@login_required
@role_required('admin')
def before_request():
    """Protect all of the admin endpoints  """
    pass


@admin.route('')
def dashboard():
    group_and_count_users = Dashboard.group_and_count_users()

    return render_template('page/dashboard.html', group_and_count_users=group_and_count_users)

@admin.route('/users')
def users():
    pass
