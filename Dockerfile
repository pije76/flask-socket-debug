FROM python:2.7-slim
MAINTAINER Aswin Ranganathan <aswinckr@gmail.com>

RUN apt-get update && apt-get install -qq -y \
  build-essential libpq-dev --no-install-recommends

ENV INSTALL_PATH /highcampers
RUN mkdir -p $INSTALL_PATH

WORKDIR $INSTALL_PATH

COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt

COPY . .
RUN pip install --editable .
#above command required for new package installation
#(done with setup.py which creates egginfo folder)

CMD gunicorn -b 0.0.0.0:9000 --worker-class eventlet -w 1 --access-logfile - "highcampers.app:create_app()"
